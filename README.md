# README #

Mongo Configs 
https://paper.dropbox.com/doc/Mongo-Configs-Wif37lAIZ80jL8lLiuKle

Runs WiredTiger storage engine.

To modify logs and data storage location, modify mongod.conf file accordingly

1. run supervisor
2. mongorestore <dump>

Installation:
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927

echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list

sudo apt-get update

sudo apt-get install -y mongodb-org